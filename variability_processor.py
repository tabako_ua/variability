import numpy as np
import os
import re
import rasterio
import json
import time

def images_to_collection(in_directory, veg_index_name):
    failures = []
    band_list = os.listdir(in_directory)

    ndvi_name_matcher = re.compile(r'\w*' + veg_index_name + '\w*.PNG')
    ndvi_name_list = {item.upper() for item in band_list if
                      (os.path.isfile(in_directory + "\\" + item) and ndvi_name_matcher.match(item.upper()))}
    json_name_matcher = re.compile(r'\w*' + veg_index_name + '\w*.json')
    json_name_list = {item.upper() for item in band_list if
                      (os.path.isfile(in_directory + "\\" + item) and json_name_matcher.match(item.upper()))}

    for item in ndvi_name_list:
        with rasterio.open(in_directory + "\\" + item, 'r') as im:
            im_array = im.read(1)  # reading raster PNG image as ONEband array
            im_array = im_array[np.nonzero(im_array)]  # clearing zero-values

            if np.count_nonzero(im_array != 254) / len(im_array) >= 0.65:
                trash_count = int(
                    np.count_nonzero(im_array != 254) * 0.95)  # calculating trash-hold level for variability index
                val_array = [{'value': i, 'count': 0} for i in range(0, 254)]

                for value in im_array:  # counting pixel_vales between unique
                    if value != 254:
                        val_array[value]['count'] = val_array[value]['count'] + 1

                val_array = sorted(val_array, key=lambda x: x['count'], reverse=True)
                check_sum = 0
                indexes = []

                for i in range(0, 254):
                    if check_sum <= trash_count:
                        check_sum = check_sum + val_array[i]['count']
                        if val_array[i]['value'] not in indexes:
                            indexes.append(val_array[i]['value'])
                    else:
                        break

                if os.path.isfile(in_directory + "\\" + item[:-4] + '_meta.json'):
                    # -------------------------------------------------------------------
                    variability_index = (255 - (max(indexes) - min(indexes))) / 255
                    # with open(in_directory + "\\" + item[:-4]+'_meta.json', 'w') as meta_json:
                    #     meta_data = json.load(meta_json)
                    #     meta_data['variability_index'] = round(variability_index, 4)
                    #     json.dump(meta_data, meta_json)
                    meta_json = open(in_directory + "\\" + item[:-4]+'_meta.json', 'rb')
                    meta_str = meta_json.read()
                    meta_data = json.loads(meta_str)
                    meta_data['variability_index'] = round(variability_index, 4)
                    meta_json.close()
                    with open(in_directory + "\\" + item[:-4] + '_meta.json', 'w') as f:
                        json.dump(meta_data, f)
                    # -------------------------------------------------------------------
                else:
                    failures.append(item)
            else:
                failures.append(item)
    return failures


if __name__ == '__main__':

    # -----------------------------------------------------------INPUT-DIRECTORY-FOR-VARIABILITY-INDEX-CALCULATIONS-----
    in_dir = 'd:\\AO-Processing\\VI_VARIABILITY\\test'
    # ------------------------------------------------------------------------------------------------------------------

    vi_list = ['NDVI', 'NDWI', 'AVI']
    failure_list = []

    for vi in vi_list:
        fails = images_to_collection(in_dir, vi)
        if len(fails) != 0:
            failure_list.append(fails)

    print()
